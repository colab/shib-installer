#!/bin/bash

echo ""
echo "Shibboleth Installer by Toolbox."
echo "--------------------------"
echo "Based on original installer and compiler by RAnsel, as well as the updated instructions by Theodore Salazar"
echo "--------------------------"
echo "This script installs Duke Shibboleth on your web server, allowing you to validate that your users are Duke students. You will be able to get their netids, names, and affiliations in your pages."
echo ""

if [[ $UID != 0 ]]; then
	echo "Please run this script with sudo:"
	echo "sudo $0 $*"
	exit 1
fi

sleep 5s;


clear 
echo "--------------------------"Directory
echo "Before running this installer, make sure that you have SSL enabled on your site. If you don't know what SSL is, you don't have it yet. Take a visit to dev.colab.duke.edu to learn how to enable SSL."
echo "--------------------------"
read -p "Press [Enter] to confirm your site uses SSL (or ctrl-c to cancel and install SSL)"

clear

echo "--------------------------"
echo "First things first. Go sign on to https://idms-web.oit.duke.edu/spreg/sps"
echo "Then, click the 'New Registration' button"
read -p "Press [Enter] When you are ready for the next step."


function pause() {   #TO BE USED FROM HERE ON
	read -p "Press [Enter] to continue... $*"
}

clear
echo "--------------------------"
echo "Now I'm going to install the required packages. Be prepared for a lot of text!"
pause

sudo apt-get -y install shibboleth-sp2-schemas libshibsp-dev libshibsp-doc libapache2-mod-shib2 opensaml2-tools curl

result="$?"

sleep 1s
echo "."
sleep 1s;
echo "."
sleep 1s;
echo "."

clear

echo "--------------------------"
if [ "0" = "$result" ]
then
	echo "Well that wasn't too bad, now was it?"
else
	echo "I was unable to install some of the required stuff. :("
	echo "Aborting now..."
	sleep 5s
	exit 10
fi

echo "Now I'm going to generate some keys for you to share with Duke's servers"
echo "These will allow you to share user login data securely with Duke."
echo "First, I need to know the domain name of your server. This is in the format of 'sbx-colab-##.oit.duke.edu' by default, but you may have set up a domain more like '####.colab.duke.edu' or '####.com'. Note that no 'http://' prefix is needed here."
echo "Write your domain here and press [Enter]: "
read domainname

shib-keygen -f "$domainname" #appears to automatically generate to the right directory

#mv -f sp-key.pem /etc/shibboleth
#mv -f sp-cert.pem /etc/shibboleth

result="$?"
sleep 2s
clear

echo "--------------------------"
if [ "0" = "$result" ]
	then
	echo "Certificate and key generated and moved into the right place."
else
	echo "I couldn't move the certificate :("
	echo "Aborting now..."
	sleep 5s
	exit 11
fi

if [ -d "/opt/bitnami" ]
then
	echo "I found a bitnami installation. I'm going to fix the libstd++ library now."
	cd /opt/bitnami/common/lib

	mv libstd++.a libstd++.a.old
	ln -s /

	#rename the library for backup
	mv libstd++.so.6* libstd++.so.*.old
	#symbolic link to real system libraries
	ln -s /usr/lib/x86_64-linux-gnu/libstdc++.so*
	#probably unnecessary, but creates generic library link to a library link that 		hopefully was just created in the directory
	ln -s libstdc++.so libstdc++.so.6
	result="$?"
	echo "--------------------------"
	if [ "0" = "$result" ]
		then
		echo "Successfully replaced library files"
	else
		echo "I wasn't able to automatically do it all. Hopefully it'll still work..."
		echo "Make sure that libstd++.so in /opt/bitnami/common/lib points to libstd++.so* in /usr/lib/x86_64-linux-gnu/libstdc++.so"
		echo "If you need help here, ask a Co-Lab assistant."
	fi
	cd ~
	pause
	clear
	echo "---------------------------"
fi

echo "Now I'm going to configure apache to use shibboleth to authenticate everything inside a particular directory on your server. This directory should be of the form 'dir', and will show up on your website as colab-sbx-#.oit.duke.edu/dir."
echo "What directory would you like to be hidden behind shibboleth? (Just use a slash to hide your entire site behind shib)"
read shibdirectory

echo "# https://wiki.shibboleth.net/confluence/display/SHIB2/NativeSPApacheConfig
# RPM installations on platforms with a conf.d directory will
# result in this file being copied into that directory for you
# and preserved across upgrades.
# For non-RPM installs, you should copy the relevant contents of
# this file to a configuration location you control.
#
# Load the Shibboleth module.
#
LoadModule mod_shib /usr/lib/apache2/modules/mod_shib_22.so
#
# Used for example style sheet in error templates.
#
<IfModule mod_alias.c>
  <Location /shibboleth-sp>
    Allow from all
  </Location>
  Alias /shibboleth-sp/main.css /usr/share/shibboleth/main.css
</IfModule>
#
# Configure the module for content.
#
# You MUST enable AuthType shibboleth for the module to process
# any requests, and there MUST be a require command as well. To
# enable Shibboleth but not specify any session/access requirements
# use 'require shibboleth'.
#
<Location /$shibdir>
  AuthType shibboleth
  ShibRequestSetting requireSession 1
  require valid-user
</Location>" > shib.conf

if [ -d "/opt/bitnami" ]
then
	echo "Detected bitnami! Moving configuration to bitnami apache instance."
	sudo mv shib.conf /opt/bitnami/apache2/conf/shib.conf
else
	echo "No bitnami detected, moving configuration to standard apache configuration directory"
	if [ -d "/etc/apache2/conf.d" ]
	then
		echo "Moving to /etc/apache2/conf.d"
		sudo mv shib.conf /etc/apache2/conf.d/shib.conf
	else
		echo "Moving to /etc/apache2/conf-enabled"
		sudo mv shib.conf /etc/apache2/conf-enabled/shib.conf
	fi
fi

result="$?"

if [ "0" = "$result" ]
	then
	echo "Configuration properly installed."
else
	echo "I couldn't find the right place for shib.conf. Move it to your apache configuration directory please. It's in the directory that you ran this script from."
	echo "If you need help, ask a Co-Lab Assistant"
fi

pause
clear
echo "---------------------------"

echo "Now we'll finally fill out that webpage you have up!"
echo "1) The entity should be 'https://$domainname/$shibdirectory'"
echo "2) Don't check the draft button."
echo "3) The public name is whatever this application is called. Just name it something!"
echo "4) The functional purpose is whatever the app is supposed to do. Just generally describe what you're building."
echo "5) The Responsible dept is 'SSI:OIT'."
echo "6) The Functional Owner Dept is 'ADS:OIT'."
echo "7) Describe the audience of this app (in like two sentences). Who will it benefit?"
echo "8) Leave environment as 'prod'. We're not going to get all fancy with multiple dev environments."
echo "9) Fill out your contact information as the technical contact in the contact box."
echo "10) Put your netid in the users box. It determines who can change this shibboleth installation."
echo "11a) The certificate box will be filled with the certificate we generated earlier. Press [ENTER] to view the certificate."

pause
clear
cat /etc/shibboleth/sp-cert.pem
echo "COPY EVERYTHING ABOVE THIS LINE (not including this line)"
echo ""
echo "11b) Now paste the certificate you just copied into the certificate box."
pause

clear
echo "---------------------------"
echo "12) Leave Binding the way it is."
echo "13) Change 'your.host' in location to $domainname."
echo "14) Leave 'Is default' as unchecked."
echo "15) Click 'add an attribute' 3 times. Change the Attribute Names to 'sn', 'DisplayName', and 'GivenName', respectively. Leave the rest of the boxes blank."
echo "16) Finally, hit Create!"
pause

clear
echo "---------------------------"
echo "The registration will take a few minutes to process, so while that is going on, let's install the last few configuration files."
pause

curl -o /etc/shibboleth/duke-metadata-2-signed.xml https://shib.oit.duke.edu/duke-metadata-2-signed.xml
curl -o /etc/shibboleth/idp_signing.crt https://shib.oit.duke.edu/idp_signing.crt

result="$?"

if [ "0" = "$result" ]
	then
	echo "Configuration files added successfully."
else
	echo "I couldn't download these files, you can try to manually download them and save them to /etc/shibboleth."
	echo "If you need help, ask a Co-Lab Assistant"
fi
pause

clear
echo "---------------------------"
echo "Now that your instance is probably registered, click on 'Registered SPs' in your web browser. Click on the Shib2 link for the registration we just filled out. This will create a Shibboleth2.xml file."
echo "The default options for everything should be okay, but double check that they all start with /etc/shibboleth."
echo "Click create!"
pause

clear
echo "---------------------------"
echo "It should spit out a webpage filled with nonsense. Copy all of that nonsense and save it as /etc/shibboleth/shibboleth2.xml on your server."
pause

clear
echo "---------------------------"
echo "You did it! Don't be discouraged if it doesn't work right immediately. You can look at the documentation on dev.colab.duke.edu for help. It might also be helpful to look inside this script and see what commands it tries to run."
echo "When you press [Enter], you'll see the status of shibboleth on your server."
pause

clear
curl -k https://localhost/Shibboleth.sso/Status
